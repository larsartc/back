import { DB_URI, DB_NAME } from '../../../../config'
import { CrawlerRunOptions } from 'rakoon/dist/models'
import { CrawlerStorage } from 'rakoon/dist/storage'
import { DrogasilCrawler } from './DrogasilCrawler'

export const StartCrawler = async () => {
   
    const storage = new CrawlerStorage(DB_URI, DB_NAME, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })

    await storage.connectToDB();

    const crawler = new DrogasilCrawler(storage);
   
    const runOptions: CrawlerRunOptions = {
        extractDetailsOptions: { // config object for the extractDetails step.
            concurrency: 10, // how many requests are made in parallel
            waitFor: 1000 * 3 // how much time the robot has to wait until another cycle of requests is made
        },
        extractLinksOptions: { // config object for the extractLinks step
            linksFromDB: false,
            concurrency: 5, // how many requests are made in parallel
            waitFor: 1000 * 3, // how much time the robot has to wait until another cycle of requests is made
            dbLinksFilter: null
        }
    };

    const links = await crawler.runExtractLinks(runOptions.extractLinksOptions);
    const result = crawler.runExtractDetails(links, runOptions.extractDetailsOptions);

    // Examples: https://www.drogasil.com.br/catalog/product/view/id/19162 | https://www.drogasil.com.br/oscal-d-400mg-com-60-compimidos.html
    // const result = await crawler.extractDetails("https://www.drogasil.com.br/catalog/product/view/id/19162")

    crawler.run(runOptions);
    console.log("Done!");

    return result;
}
