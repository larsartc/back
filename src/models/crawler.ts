import mongoose from 'mongoose';

const CrawlerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ["idle", "running", "stopped", "finished", "failed"],
        default: "idle",
        required: true,
    },
    startedAt: {
        type: Date,
        required: true
    },
    stoppedAt: {
        type: Date,
        required: true
    },
    links: {
        type: Number,
        default: 0,
        required: true
    }
});

const Crawler = mongoose.model('Crawler', CrawlerSchema);

export default Crawler;