import { Request, Response } from 'express';
import { Base64 } from 'js-base64';
import jwt from 'jsonwebtoken';

import { JWT_SECRET } from '../../config';
import User from '../models/user';

class AuthController {
	async authentication(request: Request, response: Response) {
		if(request.headers.authorization) {
			const [email, password] = Base64.decode(request.headers.authorization.split(" ")[1]).split(":");
		
			try {
				const data = await User.where('email', email).where('password', password);
				if(data) {
					const user = data[0];
					const id = user._id;

					const token = jwt.sign({ id }, JWT_SECRET, {
						expiresIn: 3600
					});

					return response.json({
						token,
						data: user
					});
				}
			} catch(e) {
				console.log(e);
				return response.status(400).json({error: "Invalid Authorization"});
			}
		}
		return response.status(400).json({error: "Invalid provided data"});
	}
}

export default AuthController;
