import { Request, Response } from 'express';

import Crawler from '../models/crawler';

import { StartCrawler } from '../services/crawler/drogasil/runCrawler';

class CrawlerController {
	async list(request: Request, response: Response) {
        try {
            const crawlers = await Crawler.find();

            return response.json(crawlers);
        } catch(e) {
            console.log(e);
            return response.status(400).json({error: "Cannot list crawlers"});
        }
    }
    
    async retrieve(request: Request, response: Response) {
        const id = request.params["id"];
        if(id) {
            const data = await Crawler.findById(id);
            if(data) {
                return response.json(data);
            } 
            return response.status(404).json({error: "Any crawler was found with this id"})
        }
        return response.status(400).json({error: "Crawler id not provided"});
    }
    
    async start(request: Request, response: Response) {
        const id = request.params["id"];
        if(id) {
            StartCrawler();

            try {
                const data = await Crawler.findByIdAndUpdate(id, {
                    status: "running",
                    startedAt: Date.now
                }, { new: true });
    
                if(data) {
                    await data?.save();

                    return response.json(data);
                }
            } catch(e) {
                console.log(e);
                return response.status(400).json({error: "Cannot update crawler"});
            }
        }
        return response.status(400).json({error: "Crawler id not provided"});
    }
}

export default CrawlerController;
