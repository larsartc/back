import express from 'express';
import cors from 'cors';

import { connect } from './database';
import routes from './routes';

const app = express();

connect();

app.use(cors());
app.use(express.json());
app.use(routes);

app.listen(process.env.PORT ?? 3333);
