import { Router } from 'express';

import AuthController from './controllers/AuthController';
import CrawlerController from './controllers/CrawlerController';

const routes = Router();

const authController = new AuthController();
const crawlerController = new CrawlerController();

routes.post('/auth', authController.authentication);

routes.get('/crawlers', crawlerController.list);
routes.get('/crawlers/:id', crawlerController.retrieve);
routes.post('/crawlers/:id/start', crawlerController.start);

export default routes;
