import mongoose from 'mongoose';
import { DB_URI } from '../../config';

let database: mongoose.Connection;

export const connect = () => {
    const uri = "";

    if(database) {
        return;
    }

    try {
        mongoose.connect(DB_URI ?? uri, {
            useNewUrlParser: true,
            useFindAndModify: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        });

        database = mongoose.connection;

        database.once('open', async () => {
            console.log('Database connected');
        });
    } catch(e) {
        console.log(e);
    }
}

export const disconnect = () => {
    if(!database) {
        return;
    }
    mongoose.disconnect();
}