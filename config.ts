import * as dotenv from 'dotenv';

dotenv.config();

export const DB_URI = process.env.DB_URI ?? "";
export const DB_NAME = process.env.DB_NAME ?? "";
export const JWT_SECRET = process.env.JWT_SECRET ?? "secret";