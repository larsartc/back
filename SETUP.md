## Setup the project

First, you need to build a docker container

```
docker build -t <container_name> .
```

After built, you need to run the container

```
docker run -p 3333:3333 -d <container_name>
```

